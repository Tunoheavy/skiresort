#!/usr/bin/env python3

__author__ = "David Verdú Ojeda"

#Contains the map as a matrix, loading from file
#Actually the matrix is represented as a list of lists of integers
class Map:
    def __init__(self, filename):   #constructor
        self.filename = filename
        file = open(filename, "r")
        values = [line.replace("\r\n", "").split(" ") for line in file] #read all lines into an matrix of strings
        values = [list(map(int, row)) for row in values]                #convert to a matrix of integers
        self.rows = values[0][0]     #first row contains the size of the map
        self.columns = values[0][1]
        self.values = values[1:]     #map is represented by the rest of the rows of the matrix


#Contains the coorditanes and value of a map point
class Point:
    def __init__(self,row,column,value): #constructor
        self.row    = row       #row or y coord
        self.column = column    #column or x coord
        self.value  = value     #cached point value to save computation time

    #overload == operator. Two points are equal if they have the same coordinates
    def __eq__(self,other):
        return self.row == other.row and self.column == other.column


#Contains a series of points on a map that stands for a path from an inital point to an end point
class Path:
    def __init__(self,map,points=[]): #constructor
        self.map = map          #map passed by reference
        self.points = points    #list of points

    #determine if a point is present in the path, performing a linear search in the list of points
    def contains_point(self,point):
        for p in self.points:
            if p == point:
                return True
        return False

    #overload > operator to allow sort and comparison. compare by length, untie by path drop
    def __gt__(self, other):
        if len(self.points) > len(other.points):
            return True
        if len(self.points) == len(other.points):
            return self.drop() > other.drop()
        return False

    #overload len. Path's length is its point list's length
    def __len__(self):
        return len(self.points)

    #compute the drop of a path, which is the difference from the highest and the lowest point
    def drop(self):
        return self.points[0].value - self.points[-1].value

    #define method to print path as 10-6-4-1-0
    def __str__(self):
        rep = "-".join([str(point.value) for point in self.points])
        return rep

    #determine which neighbour points of the last path point can be used to move forward through the map
    def next_points(self):
        neighbours = []
        current_point = self.points[-1]
        current_value = current_point.value

        #add point above if is not border and not yet included in path and its value is less than current value
        if current_point.row > 0:
            up_point = Point(current_point.row-1,current_point.column,self.map.values[current_point.row-1][current_point.column])
            if current_value > up_point.value and not self.contains_point(up_point):
                neighbours.append(up_point)

        # add point below if is not border and not yet included in path and its value is less than current value
        if current_point.row < self.map.rows - 1:
            down_point = Point(current_point.row + 1, current_point.column, self.map.values[current_point.row + 1][current_point.column])
            if current_value > down_point.value and not self.contains_point(down_point):
                neighbours.append(down_point)

        # add point on the left if is not border and not yet included in path and its value is less than current value
        if current_point.column > 0:
            left_point = Point(current_point.row,current_point.column-1, self.map.values[current_point.row][current_point.column-1])
            if current_value > left_point.value and not self.contains_point(left_point):
                neighbours.append(left_point)

        # add point on the right if is not border and not yet included in path and its value is less than current value
        if current_point.column < self.map.columns - 1:
            right_point = Point(current_point.row, current_point.column + 1, self.map.values[current_point.row][current_point.column + 1])
            if current_value > right_point.value and not self.contains_point(right_point):
                neighbours.append(right_point)

        return neighbours
