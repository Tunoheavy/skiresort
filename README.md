# SkiResort

A pieace of Python source that explores a map and determine which path is better

## Branches

This repository has 2 branches:

**master**: runs a sequential code.
**parallel**: run a parallel code.

chose the branch you prefer

## Execute with Python 3

If you have python 3 installed, run the following command

```bash
python3 skiresort.py
```

## Execute in Docker image

If you don't want to install Python 3 in your system, you can create a Docker image and run it. To create the Docker image:

```bash
docker build -t skiresort .
```

Then, to run the code in the container, enter the following command:

```
docker run -it skiresort
```
