#!/usr/bin/env python3

__author__ = "David Verdú Ojeda"

from skimap import Map, Point, Path #import required classes from skimap module
import concurrent.futures

#set to True or False to set parallel execution
run_parallel = False

#recursive function that explores all possibles paths given an initial one and returns the best solution
def find_path(path):
    valid_neighbour_points = path.next_points() #determine which neighbour points can be used to keep building paths

    #no valid neighbours mean that a complete path was found. return it by copying the list of points
    if len(valid_neighbour_points) == 0:
        return Path(path.map,path.points[:]) #path.points[:] slice method copies the list of points to not lose it on recursive return

    #we can keep building up partial paths using up to 4 neighbour valid points
    solutions = []
    for point in valid_neighbour_points:
        path.points.append(point)  #move forward in the path using the previous path list
        solution = find_path(path) #recursive call to keep searching paths and return the best solution using this point to move forward
        solutions.append(solution) #store found path in the list of solutions
        path.points.pop()          #move backwards by removing the last point of the path to keep searching solutions

    solutions.sort(reverse=True)   #we can have up to 4 paths. we return the best one by ordering and keeping the first
    return solutions[0]


#function to run as a thread
def find_path_parallel(args):
    #print(args)
    map = args[0]
    row = args[1]
    rowdata = args[2]
    thread_best_path = None
    #iterate across each column to find the thread best path
    for column, value in enumerate(rowdata):
        initial_point = Point(row, column, value)
        initial_path = Path(map, [initial_point])  # create initial path to explore from initial point
        best_path = find_path(initial_path)  # find the best path starting from this initial path

        # if the path found from this point is better than the others found, we save it
        if thread_best_path is None or best_path > thread_best_path:
            thread_best_path = best_path
    return thread_best_path

#Load map
print("loading map...")
#map = Map("4x4.txt")
map = Map("map.txt")


print("searching for the best path on map...")

#to store the best path of the map
global_best_path = None

if run_parallel == True: #parallel execution
    solutions = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        #launches a thread per map row to find best path
        for path in executor.map(find_path_parallel, [[map, row, rowdata] for row, rowdata in enumerate(map.values)]):
            solutions.append(path) #store each thread's best path
    solutions.sort(reverse=True)     #sort solutions to get best path
    global_best_path = solutions[0]

else: #sequential execution
    # Explore all initial points of the map to find the best path
    for row, rowdata in enumerate(map.values):
        for column, value in enumerate(rowdata):
            initial_point = Point(row, column, value)
            initial_path = Path(map, [initial_point])  # create initial path to explore from initial point
            best_path = find_path(initial_path)  # find the best path starting from this initial path

            # if the path found from this point is better than the others found, we save it
            if global_best_path is None or best_path > global_best_path:
                global_best_path = best_path

#print solution
print("solution found!")
print("best path:",str(global_best_path))
print("length:", len(global_best_path))
print("drop:",global_best_path.drop())
